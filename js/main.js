function RegisterObj(username, password, email, firstName, lastName) {
    {
        this.username = username;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}

function LoginObj(username, password) {
    {
        this.username = username;
        this.password = password;
    }
}

function register() {
    const loginTab = document.querySelector('#loginSwipeTab');
    const registerTab = document.querySelector('#registerSwipeTab');

    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            console.log(xhr.status);
            console.log(xhr.responseText);

            if (xhr.status === 200) {
                console.log('Uspesno ste se registrovali');
                M.toast({html: 'Registration succeseful!'});
                registerTab.classList.remove('active');
                loginTab.classList.add('active');
            } else {
                console.log('Error');
            }
        }
    };


    const username = document.querySelector('#registerUsername').value;
    const password = document.querySelector('#registerPassword').value;
    const email = document.querySelector('#email').value;
    const firstName = document.querySelector('#firstName').value;
    const lastName = document.querySelector('#lastName').value;

    let registerObj = new RegisterObj(username, password, email, firstName, lastName);
    let registerJSON = JSON.stringify(registerObj);

    const url = 'http://localhost:9090/register';

    xhr.open("POST", url);
    xhr.setRequestHeader('Content-Type', 'application/json; utf-8');
    xhr.send(registerJSON);

}

function login() {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            if (xhr.status === 200 && xhr.responseText === 'ROLE_USER') {
                window.location.href = 'addlink.html';
            } else if (xhr.status === 401) {
                M.toast({html: 'Incorrect username and/or password!'});
            }
        }
    };

    const username = document.querySelector('#loginUsername').value;
    const password = document.querySelector('#loginPassword').value;

    // let token = new LoginObj(username, password);


    console.log('username' + username);
    console.log('password' + password);

    const token = username + ':' + password;
    console.log("token" + token);
    const hash = btoa(token);
    console.log('hash code' + hash);
    const url = 'http://localhost:9090/login';
    xhr.open("GET", url);
    xhr.setRequestHeader('Authorization', 'Basic ' + hash);
    localStorage.setItem("login", hash);
    localStorage.setItem("username", username);

    xhr.send();

}

let addLink = document.querySelector('#url');

if (addLink) {
    addLink.addEventListener('input', checkLink, false);
}

function checkLink() {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            if (xhr.status === 200) {
                console.log('Usao sam u xhr');
                let chipsInit = document.querySelectorAll('.chips-initial');
                M.Chips.init(chipsInit, {
                    data: [{
                        tag: xhr.responseText,
                    }],
                });
            }
        }
    };

    const linkUrl = document.querySelector('#url').value;
    const linkUrlObj = {
        links: linkUrl
    };
    console.log(`linkUrl ${linkUrlObj}`);
    let linkUrlJson = JSON.stringify(linkUrlObj);
    console.log(`linkUrlJson ${linkUrlJson}`);
    const url = 'http://localhost:9090/addlink';


    xhr.open("POST", url);
    xhr.setRequestHeader('Content-Type', 'application/json; utf-8');
    xhr.setRequestHeader('Authorization', 'Basic ' + localStorage.getItem("login"));
    xhr.send(linkUrlJson);
    // localStorage.setItem("login", hash);
    // localStorage.setItem("username", username);
}

function submitLink() {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            if (xhr.status === 200) {
            }
        }
    };
    const linkUrl = document.querySelector('#url').value;
    let tag = document.querySelector('.chip').firstChild.data;
    console.log(tag);
    const submitObj = {
        tag: tag,
        links: linkUrl
    };
    // console.log(`linkUrl ${linkUrlObj}`);
    let sendJson = JSON.stringify(submitObj);
    // console.log(`linkUrlJson ${linkUrlJson}`);
    const url = 'http://localhost:9090/addlink';


    xhr.open("POST", url);
    xhr.setRequestHeader('Content-Type', 'application/json; utf-8');
    xhr.setRequestHeader('Authorization', 'Basic ' + localStorage.getItem("login"));
    xhr.send(sendJson);

    // let forma = document.querySelector('#linkForm');
    window.location.href = 'addlink.html';


    // const linkUrl = document.querySelector('#url').value;
    // const chipData = document.querySelectorAll('#chipTag');
    // console.log(chipData.tag);
    // const linkUrlObj = {
    //     links: linkUrl
    // };
    // console.log(`linkUrl ${linkUrlObj}`);
    // let linkUrlJson = JSON.stringify(linkUrlObj);
    // console.log(`linkUrlJson ${linkUrlJson}`);
    // const url = 'http://localhost:9090/addlink';
    //
    //
    // xhr.open("POST", url);
    // xhr.setRequestHeader('Content-Type', 'application/json; utf-8');
    // xhr.setRequestHeader('Authorization', 'Basic ' + localStorage.getItem("login"));
    // xhr.send(linkUrlJson);
}

let tabs = document.querySelector('.tabs');
M.Tabs.init(tabs, {});
M.updateTextFields();
document.addEventListener('DOMContentLoaded', function () {
    let chips = document.querySelectorAll('.chips');
    let chipsPlaceholder = document.querySelector('.chips-placeholder');

    M.Chips.init(chipsPlaceholder, {
        placeholder: 'Press "Enter"'
    });


});